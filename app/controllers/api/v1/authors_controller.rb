class Api::V1::AuthorsController < ApplicationController
    def get_authors
        authors = Author.all
        render json: authors, each_serializer: AuthorSerializer, status: 200
    end

    def get_author
        id = params[:id]
        author = Author.find_by_id(id)
        if author
            render json: author, serializer: AuthorSerializer, status: 200
        else
            render json: {status: "error", code: 3000, message: "Book not found."}
        end
    end

    def create_author
        name = params[:name]
        age = params[:age]
        dob = params[:dob]
        Author.create(name: name, age: age, dob: dob)
    end
end