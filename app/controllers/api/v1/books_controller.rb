class Api::V1::BooksController < ApplicationController
    skip_before_action :verify_authenticity_token
    def get_books
        books = Book.all
        render json: books, each_serializer: BookSerializer, status: 200
    end

    def get_book
        id = params[:id]
        book = Book.find_by_id(id)
        if book
            render json: book, serializer: BookSerializer, status: 200
        else
            render json: {status: "error", code: 3000, message: "Book not found."}
        end
    end

    def create_book
        title = params[:title]
        author_id = params[:author_id]
        cover = params[:cover]
        description = params[:description]
        book = Book.create(title: title, author_id: author_id, cover: cover, description: description)
        
        if book
            render json: book, serializer: BookSerializer, status: 200
        else
            render json: {status: "error", code: 3000, message: "Failed to create book."}
        end

    end
end