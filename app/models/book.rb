class Book < ApplicationRecord
  belongs_to :author
  has_attached_file :cover, styles: { medium: "300x300>", thumb: "100x100>" }
    do_not_validate_attachment_file_type :cover
end
