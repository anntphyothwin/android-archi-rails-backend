class AuthorSerializer < ActiveModel::Serializer
  attributes :id, :name, :age, :dob

  def dob
    object.dob.strftime("%F %T")
  end
end
