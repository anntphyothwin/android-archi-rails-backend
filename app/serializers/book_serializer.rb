class BookSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :author_id, :created_at, :cover
  def created_at
    object.created_at.strftime("%F %T")
  end
end
