Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :books
  resources :authors

  get "api/v1/getBooks" => "api/v1/books#get_books"
  get "api/v1/getBook" => "api/v1/books#get_book"
  post "api/v1/createBook" => "api/v1/books#create_book"

  get "api/v1/getAuthors" => "api/v1/authors#get_authors"
  get "api/v1/getAuthor" => "api/v1/authors#get_author"
  post "api/v1/createAuthor" => "api/v1/authors#create_author"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
